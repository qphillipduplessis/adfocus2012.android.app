/**
 * Created with IntelliJ IDEA.
 * User: Phillip du Plessis
 * Date: 2012/08/21
 * Time: 9:45 AM
 * To change this template use File | Settings | File Templates.
 */

document.ontouchmove = function(event){
    event.preventDefault();
};

/* VARIABLES */
var currentsection = "load";
var quoteinterval = null;
var bravequotecount = 7;
var curiousquotecount = 7;
var bravequotes = new Array();
var curiousquotes = new Array();
/* END VARIABLES */

function generateQuoteVal(quotetype){
    var quotetargetcount = eval(quotetype + "quotecount");
    var newquoteval = Math.floor((Math.random()*quotetargetcount) + 1);
    if(eval(quotetype + "quotes").length > 0){
        // if the array is the same length as the quote counts, then remove the first element, else generate a new value
        if(eval(quotetype + "quotes").length == quotetargetcount){
            // remove the first quote from the array - assign it to the newquoteval variable - push it to the end of the array
            newquoteval = eval("" + quotetype + "quotes")[0];
            eval(quotetype + "quotes").shift();
        } else{
            // check if the value exists in the array
            if(eval(quotetype + "quotes").indexOf(newquoteval) != -1){
                // assign a new value to newquoteval and check against the array until it does not match any values currently stored in it
                do{
                    newquoteval = Math.floor((Math.random()*quotetargetcount) + 1);
                } while(eval(quotetype + "quotes").indexOf(newquoteval) != -1);
            }
        }
    }
    eval("" + quotetype + "quotes").push(newquoteval);
    return newquoteval;
}

function changeQuotes(quotetarget, quoteval){
    var quoteContainer = document.getElementById("" + quotetarget + "QuoteContainer");
    if(eval("" + quotetarget + "quotes").length > 1)
        document.getElementById("" + quotetarget + "Quote" + eval("" + quotetarget + "quotes")[eval("" + quotetarget + "quotes").length - 2]).style.display = "none";
    document.getElementById("" + quotetarget + "Quote" + quoteval + "").style.display = "block";
    quoteContainer.className = (quoteContainer.className.indexOf("2") != -1) ? "" + quotetarget + "Quote" : "" + quotetarget + "Quote2";
}

function contentQuotes(){
    var bravequote = generateQuoteVal("brave");
    var curiousquote = generateQuoteVal("curious");
    changeQuotes("brave", bravequote);
    changeQuotes("curious", curiousquote);
    if(quoteinterval == null || typeof(quoteinterval) == "undefined")
        quoteinterval = setInterval(contentQuotes, 5000);
}

function setContent(targetcontent){
    if(targetcontent == null || typeof(targetcontent) == "undefined")
        targetcontent = "splash";
    document.getElementById("" + targetcontent + "page").style.display = "block";
    document.getElementById("" + currentsection + "page").style.display = "none";
    if(targetcontent == "content")
        contentQuotes();
    if(currentsection == "content"){
        clearInterval(quoteinterval);
        quoteinterval = null;
    }
    currentsection = targetcontent;
}

function initElements(){
    setContent("showreel");
}

function init(){
    document.addEventListener("deviceready", initElements, true);
    //initElements();
}